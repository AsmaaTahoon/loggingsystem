# LoggingSystem

simple logging system which have an API endpoints to send logs and dashboard to view those logs.

This system is built using Hapi Js framework, Angular 6, Mongodb, and swagger api to build APIs documentation

#### Running the Application Locally
1. Install Node.js (10 or higher) and MongoDB on your dev box
2. Open Server/config/config.js and write your mongoDb url and change host to be localhost
3. Install Nodemon: ``npm install nodemon -g``
4. Run ``npm install`` to install app dependencies
5. Run ``npm run start`` to start the server
6. Go to /Client-log-system and run ``npm install`` to install angular dependencies
7. Run ``ng serve`` start angular server
8. Open the system by typing ``localhost:4200`` at browser
9. Open the API documentation by typing ``localhost:3000/documentation/`` at browser
   


#### Running the Application with Docker
1. Open Server/config/config.js and write your mongod database name
2. Go to Client-log-system and run ``sudo docker build -t log-system-client:dev .``
3. Go to Server and run ``sudo docker build -t log-system-server:dev .``
4. Run ``docker run -d --name mongodb -p 27017:27017 mongo``
5. At root directory run ``docker-compose up``
6. Open the system by typing ``localhost:4200`` at browser
7. Open the API documentation by typing ``localhost:3000/documentation/`` at browser