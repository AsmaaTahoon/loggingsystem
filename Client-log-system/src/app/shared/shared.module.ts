import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StartComponent } from './component/start/start.component';
import { RouterModule } from "@angular/router";

@NgModule({
  declarations: [StartComponent],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class SharedModule { }
