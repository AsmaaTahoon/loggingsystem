import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import {environment} from "../../environments/environment";

const httpOptions = {headers: new HttpHeaders({'Content-Type':  'application/json'})};
const API_URL = environment.apiURL;

@Injectable({
  providedIn: 'root'
})
export class LogService {

  constructor(private http: HttpClient) { }

    public postLogAction(data) {
        return this.http.post(API_URL + '/log', data, httpOptions )
    }

    public getLogsAction(page, limit, searchQuery?) {
        return this.http.get(API_URL + '/logs?page='+page+'&limit='+limit+searchQuery)
    }

    public getLogAction(id) {
        return this.http.get(API_URL + '/log/'+id)
    }

}
