import { Component, OnInit } from '@angular/core';
import {LogService} from "../../log.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.css']
})
export class LogComponent implements OnInit {

  logId;
  log;

  constructor(private logService: LogService, private route: ActivatedRoute) {}

  ngOnInit() {
      this.route.params.subscribe(params => {
          this.logId = params['id'];
          if (this.logId) {
            this.getlog();
          }
      });
  }

  getlog() {
    this.logService.getLogAction(this.logId).subscribe(response => {
      this.log = response;
    });
  }

}
