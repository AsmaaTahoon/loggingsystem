import { Component, OnInit } from '@angular/core';
import {LogService} from "../../log.service";

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css']
})
export class LogsComponent implements OnInit {

  logs:any;
  totalLogsCount;
  itemsPerPage: number = 5;
  page: number = 1;
  searchTitle;
  searchDescription;
  searchStatusCode;

  constructor(private logService: LogService) { }

  ngOnInit() {
    this.getLogsPaginated();
  }

  getLogsPaginated() {
    let searchQuery = this.getSearchQuery();
    this.logService.getLogsAction(this.page, this.itemsPerPage, searchQuery).subscribe(response => {
      this.logs = response['docs'];
      this.totalLogsCount = response['total'];
      this.page = response['page'];
    });
  }

  pageChanged($event) {
    this.page = $event;
    this.getLogsPaginated();
  }

  getSearchQuery() {
      let searchQuery = '';
      if (this.searchTitle) searchQuery = searchQuery+'&title='+this.searchTitle;
      if (this.searchDescription) searchQuery = searchQuery+'&description='+this.searchDescription;
      if (this.searchStatusCode) searchQuery = searchQuery+'&status_code='+this.searchStatusCode;

      return searchQuery;
  }

}
