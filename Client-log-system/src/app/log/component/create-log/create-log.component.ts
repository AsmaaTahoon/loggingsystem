import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {LogService} from "../../log.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-log',
  templateUrl: './create-log.component.html',
  styleUrls: ['./create-log.component.css']
})
export class CreateLogComponent implements OnInit {

    logForm: FormGroup;

    title: FormControl;
    description: FormControl;
    status_code: FormControl;
    path: FormControl;

    constructor(private formBuilder: FormBuilder, private logService: LogService, private router:Router) { }

    ngOnInit() {
      this.createLogFormControls();
      this.initLogForm();
    }

    initLogForm() {
        this.logForm = this.formBuilder.group({
            title: this.title,
            description: this.description,
            status_code: this.status_code,
            path: this.path,
        });

        return this.logForm;
    }

    createLogFormControls() {
        this.title        = new FormControl('', [Validators.required]);
        this.description  = new FormControl('', [Validators.required]);
        this.status_code  = new FormControl('', [Validators.required]);
        this.path         = new FormControl('', [Validators.required]);
    }

    createLog() {
      this.validateAllFormFields(this.logForm);
      if (this.logForm.valid) {
          this.logService.postLogAction(this.logForm.value).subscribe(response => {
              if (!response['message']) {
                  this.router.navigate(['/logs']);
              }
          })
      }
    }

    validateAllFormFields(formGroup: FormGroup) {         //{1}
        Object.keys(formGroup.controls).forEach(field => {  //{2}
            const control = formGroup.get(field);             //{3}
            if (control instanceof FormControl) {             //{4}
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {        //{5}
                this.validateAllFormFields(control);            //{6}
            }
        });
    }


}
