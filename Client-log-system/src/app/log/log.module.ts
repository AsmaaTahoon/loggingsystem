import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateLogComponent } from './component/create-log/create-log.component';
import { LogsComponent } from './component/logs/logs.component';
import { LogService } from "./log.service";
import { LogComponent } from './component/log/log.component';
import { RouterModule } from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [CreateLogComponent, LogsComponent, LogComponent],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    FormsModule
  ],
  providers: [
      LogService
  ]
})
export class LogModule { }
