import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CreateLogComponent} from "./log/component/create-log/create-log.component";
import {LogsComponent} from "./log/component/logs/logs.component";
import {StartComponent} from "./shared/component/start/start.component";
import {LogComponent} from "./log/component/log/log.component";

const routes: Routes = [
    { path: '', component: StartComponent },
    { path: 'log/add', component: CreateLogComponent },
    { path: 'logs', component: LogsComponent },
    { path: 'log/:id', component: LogComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
