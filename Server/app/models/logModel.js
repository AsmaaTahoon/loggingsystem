var Mongoose            = require('mongoose');
var Schema              = Mongoose.Schema;
var mongoosePaginate    = require('mongoose-paginate');

const logSchema = new Schema({
    title: String,
    description: String,
    status_code: Number,
    path: String,
    created_at: { type: Date, default: Date.now }
});

logSchema.plugin(mongoosePaginate);

logSchema.pre('save', function (next) {
    if (this.isNew) this.created_at = new Date;
    next();
});

const LogModel = Mongoose.model("log", logSchema);

module.exports = LogModel;