const LogModel = require('../models/logModel');

module.exports = {

    async create(request, reply) {
        try {
            var log = new LogModel(request.payload);
            var result = await log.save();
            return reply.response(result);
        } catch (error) {
            return reply.response(error).code(500);
        }
    },

    async getAll(request, reply) {
        try {
            let logs;
            await LogModel.paginate(searchQueries(), { page: request.query.page? parseInt(request.query.page): 1, limit: request.query.limit? parseInt(request.query.limit): 5, sort: {created_at: 'desc'} }, function(err, result) {
                logs = result;
            });
            return reply.response(logs);
        } catch (error) {
            return reply.response(error).code(500);
        }

        function searchQueries() {
            searchObject = {};
            if (request.query.title) {
                searchObject.title = {$regex:  request.query.title, $options: 'i'};
            }
            if (request.query.description) {
                searchObject.description = {$regex:  request.query.description, $options: 'i'};
            }
            if (request.query.status_code) {
                searchObject.status_code = request.query.status_code;
            }
            return searchObject;
        }

    },

    async getOne(request, reply) {
        try {
            var log = await LogModel.findById(request.params.id).exec();
            return reply.response(log);
        } catch (error) {
            return reply.response(error).code(500);
        }
    }
};