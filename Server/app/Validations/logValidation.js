const Joi = require('joi');

module.exports = {

    postValidate: {
        payload: {
            title: Joi.string().required(),
            description: Joi.string().required(),
            status_code: Joi.number().required(),
            path: Joi.string().required(),
        },
        failAction: (request, h, error) => {
            return error.isJoi ? h.response(error.details[0]).takeover() : h.response(error).takeover();
        }
    },

    getOneValidate: {
        params: {
            id: Joi.string().required(1)
        },
        failAction: (request, h, error) => {
            return error.isJoi ? h.response(error.details[0]).takeover() : h.response(error).takeover();
        }
    }
};