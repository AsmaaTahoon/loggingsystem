const LogController = require('../controllers/logController');
const Validation    = require('../Validations/logValidation');

module.exports = function(server) {

    server.route({
        method: "POST",
        path: "/log",
        options: {validate: Validation.postValidate, tags: ['api'],},
        handler: LogController.create
    });

    server.route({
        method: "Get",
        path: "/logs",
        options: {tags: ['api']},
        handler: LogController.getAll
    });

    server.route({
        method: "Get",
        path: "/log/{id}",
        options: {tags: ['api'], validate: Validation.getOneValidate},
        handler: LogController.getOne
    });

};