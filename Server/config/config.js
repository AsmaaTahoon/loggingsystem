module.exports = {

    mongourl : 'mongodb://database/logsys',
    port: 3000,
    host: '0.0.0.0',
    routes: {
        cors: true
    }

};