const Hapi          = require('hapi');
const Mongoose      = require('mongoose');
const Inert         = require('inert');
const Vision        = require('vision');
const HapiSwagger   = require('hapi-swagger');
const Pack          = require('./package');

var config = require('./config/config');
Mongoose.connect(config.mongourl, {useNewUrlParser: true });

(async () => {
    const server = await new Hapi.Server({
        host: config.host,
        port: config.port,
        routes: config.routes
    });

    const swaggerOptions = {
        info: {
            title: 'Loggin system API Documentation',
            version: Pack.version,
        },
    };

    await server.register([
        Inert,
        Vision,
        {
            plugin: HapiSwagger,
            options: swaggerOptions
        }
    ]);

    try {
        await server.start();
        console.log('Server running at:', server.info.uri);
    } catch(err) {
        console.log(err);
    }

    require('./app/routes/routes.js')(server);

})();